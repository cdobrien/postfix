%{
#include <stdio.h>
#include <stdlib.h>
#include "y.tab.h"
%}

%option noyywrap

%%

[0-9]+      { yylval.atom = atoi(yytext); return ATOM; }
[+*]        { return yytext[0]; }
[ \t\f\v\n] { ; }

%%
