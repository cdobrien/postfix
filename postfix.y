%{
#include <stdio.h>
#include <stdlib.h>

extern int yylex();
void yyerror(char *msg);
%}

%union {
    int atom;
}

%token <atom> ATOM
%type <atom> E F

%%

S : E       { printf("%d\n", $1); }
  ;

E : E E '+' { $$ = $1 + $2; }
  | F
  ;

F : E E '*' { $$ = $1 * $2; }
  | ATOM
  ;

%%

void yyerror(char *msg)
{
    fprintf(stderr, "%s\n", msg);
    exit(1);
}

int main()
{
    yyparse();
    return 0;
}
