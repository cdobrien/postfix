.PHONY: clean

postfix: y.tab.c lex.yy.c
	cc y.tab.c lex.yy.c -o postfix

y.tab.c: postfix.y
	yacc -d $<

lex.yy.c: postfix.l y.tab.c
	lex $<

clean:
	rm -f y.tab.* lex.yy.c postfix
